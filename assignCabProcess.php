<?php

class AssignCab {

    private $dbServer;
    private $dbName;
    private $dbUserName;
    private $dbPassword;
    private $databaseConnection;

    // Function to form a database connection connection 
    public function __construct($dbServer, $dbName, $dbUserName, $dbPassword) {
        $this->dbServer = $dbServer;
        $this->dbName = $dbName;
        $this->dbUserName = $dbUserName;
        $this->dbPassword = $dbPassword;
    }

    // Function which when called opens a database connection , runs a query to change the status for the provided booking reference, Sends the message to client side and closes database connection 
    public function assignCab() {

        $this->openDatabaseConnection();
        $this->selectDatabase();

        $bookingRefNumber = $_POST["refNumber"];
        $query = "Update bookingInformation SET status = 'assign' where bookingReferenceNumber='$bookingRefNumber'";

        $result = mysqli_query($this->databaseConnection, $query);

        if (!$result) {
            echo "<p> Something is wrong with", $query, "</p>";
        } else {
            $data = ['bookingReferenceNumber' => $bookingRefNumber];
            header('Content-Type: application/json');
            echo json_encode($data);
        }

        $this->closeDatabaseConnection();
    }

    // Function to open database connection connection 
    private function openDatabaseConnection() {
        $this->databaseConnection = mysqli_connect($this->dbServer, $this->dbUserName, $this->dbPassword, $this->dbName);
    }

    // Function to close database connection connection 
    private function closeDatabaseConnection() {
        mysqli_close($this->databaseConnection);
    }

    // Function to select database  
    private function selectDatabase() {
        mysqli_select_db($this->databaseConnection, $this->dbName)
                or die('Database not available');
    }

}

//provide database server, database name,database username, database password
$assignCab = new AssignCab('', '', '', '');
$assignCab->assignCab();
