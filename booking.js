//    Author - Maitry Mehta - 17964028
//   It is a javascript file which validates all the user input fields before sending to the server. 
//   Then passes client side request to server for processing.
//   On successful processing of file , automated success message is generated.


// Function to validate cutomer name 
function isCustomerNameValid() {
    var customerName = $("#customerName").val();
    var validationDiv = $("#customerNameError");
    if (customerName) {
        validationDiv.empty();
        return customerName;
    }
    validationDiv.html("Customer Name is required");
}


//  Function to validate ContactNumber

function isContactNumberValid() {
    var contactNumber = $("#contactNumber").val();
    var validationDiv = $("#contactNumberError");
    if (contactNumber) {
        validationDiv.empty();
        return contactNumber;
    }
    validationDiv.html("Contact Number is required");
}

// Function to validate PickUpAddress

function isPickUpAddressValid() {
    var pickUpAddress = $("#pickUpAddress").val();
    var validationDiv = $("#pickUpAddressError");
    if (pickUpAddress) {
        validationDiv.empty();
        return pickUpAddress;
    }
    validationDiv.html("PickUp Address is required");
}

// Function to validate PickUpDateAndTime

function isPickUpDateAndTimeValid() {
    var currentDateTime = new Date();
    var dateTime = $("#dateTime").val();
    var validationDiv = $("#dateTimeError");
    var userPickUpDateTime = new Date(dateTime);
    if (userPickUpDateTime >= currentDateTime) {
        validationDiv.empty();
        return dateTime;
    }
    validationDiv.html("Please provide valid date and time for pick up");
}

// Function fires an ajaxs call 
function submitBookingForm(event) {
    event.preventDefault();
    var validCustomerName = isCustomerNameValid();
    var validContactNumber = isContactNumberValid();
    var validPickUpAddress = isPickUpAddressValid();
    var validPickUpdateTime = isPickUpDateAndTimeValid();

    var type = "post";
    var url = "cabBookingProcess.php";
    var data = {
        customerName: validCustomerName,
        contactNumber: validContactNumber,
        pickUpAddress: validPickUpAddress,
        pickUpDateTime: validPickUpdateTime
    };

    if (validCustomerName && validContactNumber && validPickUpAddress && validPickUpdateTime) {
        restClient(type, url, data)
                .success(function (data) {
                    return submitBookingFormSuccess(data);
                })
                .fail(function (fail) {
                    console.log(fail);
                });
    }
}

// Function passes message in successful ajax call
function submitBookingFormSuccess(data) {
    var successDiv = $("#success");
    successDiv.html("Thank you! Your booking reference number is " + data.bookingReferenceNumber + "." +
            " You will be picked up in front of your provided address at " + data.pickUpDateTime);
}