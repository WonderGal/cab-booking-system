/*    Author - Maitry Mehta - 17964028 
  This is a javascript file for Ajax Implementation
  The client side displays the returned information from the query in the table form on admin.htm */




// Function on click calls ajax Ajax Implementation 
function onClickShowPickUpRequest(event) {
    event.preventDefault();
    var type = "get";
    var url = "pickUpProcess.php";
    var data = {};
    restClient(type, url)
            .success(function (data) {
                return onSuccess(data);
            })
            .fail(function (fail) {
                console.log(fail);
            });
}

// Function on success of ajax call shows the result in table form on client side 
function onSuccess(data) {
    $('#result').show();

    var len = data.length;
    for (var i = 0; i < len; i++) {

        var bookingReferenceNumber = data[i].bookingReferenceNumber;
        var contactNumber = data[i].contactNumber;
        var customerName = data[i].customerName;
        var generatedDate = data[i].generatedDate;
        var pickUpAddress = data[i].pickUpAddress;
        var pickUpDateTime = data[i].pickUpDateTime;
        var status = data[i].status;

        $("#result").append(
                "<tr>" +
                "<td>" + bookingReferenceNumber + "</td>" +
                "<td>" + contactNumber + "</td>" +
                "<td>" + customerName + "</td>" +
                "<td>" + generatedDate + "</td>" +
                "<td>" + pickUpAddress + "</td> " +
                "<td>" + pickUpDateTime + "</td>" +
                "<td>" + status + "</td>" +
                "</tr>"
                );
    }
}