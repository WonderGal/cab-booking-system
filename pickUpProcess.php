<?php

class SearchBooking {

    private $dbServer;
    private $dbName;
    private $dbUserName;
    private $dbPassword;
    private $databaseConnection;

    // Function to form a database connection connection 
    public function __construct($dbServer, $dbName, $dbUserName, $dbPassword) {
        $this->dbServer = $dbServer;
        $this->dbName = $dbName;
        $this->dbUserName = $dbUserName;
        $this->dbPassword = $dbPassword;
    }

    // Function which when called opens a database connection , Selects database, runs a query to select all the data of the request which are unassigned and pickup time is  within 2 hours,
    //  Sends the data of all such result in the form of table to client side and closes database connection 
    public function searchBooking() {

        $this->openDatabaseConnection();
        $this->selectDatabase();

        $query = "SELECT * FROM bookingInformation where status='unassigned' AND pickUpDateTime < (NOW() + INTERVAL 2 HOUR)";

        $result = mysqli_query($this->databaseConnection, $query);

        if (!$result) {
            echo "<p> Something is wrong with", $query, "</p>";
        } else {
            $jsonData = array();
            while ($array = mysqli_fetch_assoc($result)) {
                $jsonData[] = $array;
            }
            header('Content-Type: application/json');
            echo json_encode($jsonData);
        }

        $this->closeDatabaseConnection();
    }

    // Function to open database connection connection 
    private function openDatabaseConnection() {
        $this->databaseConnection = mysqli_connect($this->dbServer, $this->dbUserName, $this->dbPassword, $this->dbName);
    }

    // Function to close database connection connection 
    private function closeDatabaseConnection() {
        mysqli_close($this->databaseConnection);
    }

// Function to select database  
    private function selectDatabase() {
        mysqli_select_db($this->databaseConnection, $this->dbName)
                or die('Database not available');
    }

}

//provide database server,database name, database username, database password
$searchBooking = new SearchBooking('', '', '', '');
$searchBooking->searchBooking();
