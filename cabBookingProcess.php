<?php

class CabBooking {

    private $dbServer;
    private $dbUserName;
    private $dbPassword;
    private $databaseConnection;

    /* Function to form a database connection connection */

    public function __construct($dbServer, $dbName, $dbUserName, $dbPassword) {
        $this->dbServer = $dbServer;
        $this->dbName = $dbName;
        $this->dbUserName = $dbUserName;
        $this->dbPassword = $dbPassword;
    }

    //Function which when called opens a database connection , Selects database creates a table if not exists, Adds user value to the database , Sends the value to client side and closes database connection.
    public function submitStatus() {
        $this->openDatabaseConnection();
        $this->selectDatabase();
        $createTable = "CREATE TABLE IF NOT EXISTS bookingInformation(
                      bookingReferenceNumber INT(11) UNSIGNED AUTO_INCREMENT PRIMARY KEY, 
                      customerName VARCHAR(255),
                      contactNumber INT(11),
                      pickUpAddress VARCHAR(255),
                      pickUpDateTime DATETIME,
                      generatedDate TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
                      status VARCHAR(255) DEFAULT 'Unassigned'
                 )";
        $insertTable = mysqli_query($this->databaseConnection, $createTable);
        if (!$insertTable) {
            echo "<p>Something is wrong while creating table .</p>";
        }

        $customerName = $_POST["customerName"];
        $contactNumber = $_POST["contactNumber"];
        $pickUpAddress = $_POST["pickUpAddress"];
        $pickUpDateTime = $_POST["pickUpDateTime"];

        $insertValues = "INSERT INTO bookingInformation 
            (customerName, contactNumber, pickUpAddress, pickUpDateTime)  VALUES
                    ('$customerName','$contactNumber','$pickUpAddress','$pickUpDateTime')";

        $insertResult = mysqli_query($this->databaseConnection, $insertValues);

        if (!$insertResult) {
            echo "Something is wrong with ", $insertValues;
        } else {
            $bookingReferenceNumber = mysqli_insert_id($this->databaseConnection);
            $data = ['bookingReferenceNumber' => $bookingReferenceNumber, 'pickUpDateTime' => $pickUpDateTime];
            header('Content-Type: application/json');
            echo json_encode($data);
        }

        $this->closeDatabaseConnection();
    }

    // Function to open database connection connection
    private function openDatabaseConnection() {
        $this->databaseConnection = mysqli_connect($this->dbServer, $this->dbUserName, $this->dbPassword);
    }

    // Function to close database connection connection 
    private function closeDatabaseConnection() {
        mysqli_close($this->databaseConnection);
    }

    // Function to select database 
    private function selectDatabase() {
        mysqli_select_db($this->databaseConnection, "")//provide database name
                or $this->createDatabase();
    }

    // Function to create database if doesnot exist 
    private function createDatabase() {
        $createDatabase = "CREATE DATABASE ";
        mysqli_query($this->databaseConnection, $createDatabase);
        $this->selectDatabase();
    }

}

// provide database server,database name, database username, database password
$cabBooking = new CabBooking('', '', '', '');
$cabBooking->submitStatus();
