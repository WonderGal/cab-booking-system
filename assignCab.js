//    Author - Maitry Mehta - 17964028
//    This is a javascript file which validates the user reference input before sending to server.
// Then passes client side request to server for processing.



// Function validate the booking reference before passing to server to server.
function isBookingRefEmpty() {
    var bookCab = $("#bookCab").val();
    var validationDiv = $("#bookCabError");
    if (bookCab) {
        validationDiv.empty();
        return bookCab;
    }
    validationDiv.html("Book Reference number is required");
}

// Function to update status to assign in database when booking reference is passed to server.
function onClickAssignCab(event) {
    event.preventDefault();

    var refNumber = isBookingRefEmpty();

    var type = "post";
    var url = "assignCabProcess.php";
    var data = {
        refNumber: refNumber
    };

    if (refNumber) {
        restClient(type, url, data)
                .success(function (data) {
                    assignCabSuccess(data);
                })
                .fail(function (fail) {
                    console.log(fail);
                });
    }
    return;
}

// Function on success send a message to client side 
function assignCabSuccess(data) {
    var successDiv = $("#success");
    successDiv.html("The cab is assigned for Booking Reference Number: " + data.bookingReferenceNumber);
}

