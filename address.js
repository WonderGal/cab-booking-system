/**
 * Author - Maitry Mehta - 17964028
 * This js file uses the address finder API
 */

/**
 * This function is to initialize adressfinder and help the user wih adress predictions 
 * This addresses are pre-validated.
 * @returns {pickUpAdress}
 */
(function () {
    var widget, initAF = function () {
        widget = new AddressFinder.Widget(
                document.getElementById('pickUpAddress'),
                'TBLFPGEX4639UNJ8DWY7',
                'NZ',
                {show_locations: false}
        );
        widget.on("result:select", function (fullAddress, metaData) {
            var selected = new AddressFinder.NZSelectedAddress(fullAddress, metaData);
            document.getElementById('unitNo').value = selected.address_line_1();
            document.getElementById('streetName').value = selected.address_line_2();
            document.getElementById('suburb').value = selected.suburb();
            document.getElementById('city').value = selected.city();
            document.getElementById('postcode').value = selected.postcode();
        });
    };

/**
 * This function is to download adressfinder API
 */
    function downloadAF(f) {
        var script = document.createElement('script');
        script.src = 'https://api.addressfinder.io/assets/v3/widget.js';
        script.async = true;
        script.onload = f;
        document.body.appendChild(script);
    }

    document.addEventListener('DOMContentLoaded', function () {
        downloadAF(initAF);
    });
})();

