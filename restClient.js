/* Author - Maitry Mehta - 17964028 
 * This is a javascript file for Ajax Implementation
 */

function restClient(type, url, data) {
    return $.ajax({
        type: type,
        url: url,
        data: data,
        success: function (data) {
            return data;
        },
        error: function (error) {
            return error;
        }
    });
}